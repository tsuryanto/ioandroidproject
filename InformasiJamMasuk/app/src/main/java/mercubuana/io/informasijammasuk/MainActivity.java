package mercubuana.io.informasijammasuk;

import android.app.TimePickerDialog;
import android.content.ClipData;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;

import java.io.File;
import java.io.IOException;
import java.util.Calendar;

public class MainActivity extends AppCompatActivity {

    RelativeLayout layout_photo_add;
    EditText mEditTextTime;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        TextView text_edit   = findViewById(R.id.text_edit);
        text_edit.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View view) {
                showTimeDialog(1);
            }
        });

        layout_photo_add = findViewById(R.id.layout_photo_add);

        layout_photo_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ClickImageFromCamera();
            }
        });
    }

    private static final int ACTIVITY_START_CAMERA_APP = 102;
    private String mImageFileLocation;

    public void ClickImageFromCamera() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            Uri photoURI = null;
            try {
                File photoFile     = createImageFile();
                mImageFileLocation = photoFile.getAbsolutePath();
                photoURI = FileProvider.getUriForFile(getApplicationContext(),
                        getString(R.string.file_provider_authority),
                        photoFile);

            } catch (IOException ex) {
                Log.e("TakePicture", ex.getMessage());
            }
            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
            if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.LOLLIPOP) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    takePictureIntent.setClipData(ClipData.newRawUri("", photoURI));
                }
                takePictureIntent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
            }
            startActivityForResult(takePictureIntent, ACTIVITY_START_CAMERA_APP);
        }
    }

    File createImageFile() throws IOException {

        final String timestamp = "BIOSIS001";
        final String imageFileName = "JPEG_" + timestamp;
        File storageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "pics");
        storageDir.mkdirs();
        return File.createTempFile(imageFileName, ".jpg", storageDir);

    }

    private void showTimeDialog(int numbDataInput) {

        int mHour, mMinute;

        final Calendar c = Calendar.getInstance();
        mHour            = c.get(Calendar.HOUR_OF_DAY);
        mMinute          = c.get(Calendar.MINUTE);

        TimePickerDialog timePicker = getTimePickerDialog(mHour, mMinute, numbDataInput);
        timePicker.show();
    }

    private TimePickerDialog getTimePickerDialog(int mHour, int mMinute, final int numbDataInput) {
        // Launch Time Picker Dialog
        TimePickerDialog timePickerDialog = new TimePickerDialog(this,
                new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {

                        String time =  (hourOfDay < 10 ? "0" : "") + hourOfDay + ":"
                                + (minute < 10 ? "0" : "") + minute;

                        mEditTextTime.setText(time);
                    }
                }, mHour, mMinute, false);
        return timePickerDialog;
    }
}
