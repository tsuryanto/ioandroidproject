package mercubuana.io.timedifferenceapp;

import android.app.TimePickerDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class MainActivity extends AppCompatActivity {

    EditText mEditTextTime1, mEditTextTime2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mEditTextTime1       = findViewById(R.id.input_time_1);
        mEditTextTime2       = findViewById(R.id.input_time_2);
        TextView text_edit_1   = findViewById(R.id.text_edit_1);
        TextView text_edit_2   = findViewById(R.id.text_edit_2);
        
        Button btnCount     = findViewById(R.id.btn_count);

        btnCount.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View view) {
                String time1 = mEditTextTime1.getText().toString();
                String time2 = mEditTextTime2.getText().toString();
                countTimeDifference(time1, time2);
            }
        });
        
        text_edit_1.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View view) {
                showTimeDialog(1);
            }
        });

        text_edit_2.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View view) {
                showTimeDialog(2);
            }
        });
    }

    private void countTimeDifference(String time1, String time2) {
        String dateStart = "00/00/0000 " + time1 + ":00";
        String dateStop = "00/00/0000 " + time2 + ":00";

        //HH converts hour in 24 hours format (0-23), day calculation
        SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");

        Date d1 = null;
        Date d2 = null;

        try {
            d1 = format.parse(dateStart);
            d2 = format.parse(dateStop);

            //in milliseconds
            long diff = d2.getTime() - d1.getTime();

            long diffMinutes = TimeUnit.MILLISECONDS.toMinutes(diff);
            long diffHours = TimeUnit.MILLISECONDS.toHours(diff);


//            TextView tvJam = findViewById(R.id.jam);
            TextView tvMenit = findViewById(R.id.menit);

            tvMenit.setText(diffMinutes + " menit ");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showTimeDialog(int numbDataInput) {

        int mHour, mMinute;

        final Calendar c = Calendar.getInstance();
        mHour            = c.get(Calendar.HOUR_OF_DAY);
        mMinute          = c.get(Calendar.MINUTE);

        TimePickerDialog timePicker = getTimePickerDialog(mHour, mMinute, numbDataInput);
        timePicker.show();
    }

    private TimePickerDialog getTimePickerDialog(int mHour, int mMinute, final int numbDataInput) {
        // Launch Time Picker Dialog
        TimePickerDialog timePickerDialog = new TimePickerDialog(this,
                new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {

                        String time =  (hourOfDay < 10 ? "0" : "") + hourOfDay + ":"
                                + (minute < 10 ? "0" : "") + minute;

                        if (numbDataInput == 1){
                            mEditTextTime1.setText(time);
                        } else {
                            mEditTextTime2.setText(time);
                        }
                    }
                }, mHour, mMinute, false);
        return timePickerDialog;
    }
}
