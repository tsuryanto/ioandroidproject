package mercubuana.io.tasktimer;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class MainActivity extends AppCompatActivity {

    private static final String DATA_NOT_FOUND = "00.00";
    private static final String SP_TIME = "timer";
    private static final String TIME_START = "time_start";
    private static final String TIME_FINISH = "time_finish";

    SharedPreferences mTime;
    SharedPreferences.Editor spTimeEditor;

    Button btnStart, btnFinish;
    TextView tvTimeStart, tvTimeFinish;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // Buat Shared Preferences
        mTime = this.getSharedPreferences(SP_TIME, Context.MODE_PRIVATE);
        // Buat Objek Editor
        spTimeEditor = mTime.edit();

        tvTimeStart = findViewById(R.id.tv_time_start);
        tvTimeFinish = findViewById(R.id.tv_time_finish);

        btnStart = findViewById(R.id.btn_start);
        btnFinish = findViewById(R.id.btn_finish);

        tvTimeStart.setText(mTime.getString(TIME_START, DATA_NOT_FOUND));

        btnStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
                String currentTime = sdf.format(Calendar.getInstance().getTime());

                // menambah data ke field
                spTimeEditor.putString(TIME_START, currentTime);
                spTimeEditor.apply();
                tvTimeStart.setText(mTime.getString(TIME_START, DATA_NOT_FOUND));
            }
        });

        btnFinish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
                String currentTime = sdf.format(Calendar.getInstance().getTime());

                spTimeEditor.putString(TIME_FINISH, currentTime);
                spTimeEditor.apply();
                tvTimeFinish.setText(mTime.getString(TIME_FINISH, DATA_NOT_FOUND));

                countTimeDifference(
                        mTime.getString(TIME_START, DATA_NOT_FOUND),
                        mTime.getString(TIME_FINISH, DATA_NOT_FOUND)
                );
            }
        });

    }

    private void countTimeDifference(String time1, String time2) {
        String dateStart = "00/00/0000 " + time1;
        String dateStop = "00/00/0000 " + time2;

        //HH converts hour in 24 hours format (0-23), day calculation
        SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");

        Date d1 = null;
        Date d2 = null;

        try {
            d1 = format.parse(dateStart);
            d2 = format.parse(dateStop);

            //in milliseconds
            long diff = d2.getTime() - d1.getTime();

            long diffMinutes = TimeUnit.MILLISECONDS.toMinutes(diff);
            long diffHours = TimeUnit.MILLISECONDS.toHours(diff);


//            TextView tvJam = findViewById(R.id.jam);
            TextView tvMenit = findViewById(R.id.menit);

            tvMenit.setText(diffMinutes + " menit ");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
